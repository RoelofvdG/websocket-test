let ws;
let player;
let status = "paused";
let name = null;

function setupSocket() {
	console.log("Document ready");

	player = videojs("videojs-player", {
		autoplay: false,
		preload: true,
	});
	player.currentMs = function () {
		return Math.round(player.currentTime() * 1000);
	};

	player.on("play", () => {
		// console.log("User pressed play");
		if (status === "paused") {
			player.pause();
			ws.send("video.play");
		}
	});

	player.on("pause", () => {
		// console.log("User pressed pause");
		if (status === "playing") {
			player.play();
			ws.send("video.pause");
		}
	});

	player.on("seeking", (e) => {
		let time = player.currentMs();
		// console.log("Seeking " + time + "ms");
		if (player.scrubbing()) {
			ws.send("video.pause");
			// console.log("User changing time");
			let time = player.currentMs();
			ws.send(`video.seekMs.${time}`);
		}
	});

	ws.addEventListener("message", (e) => {
		let msg = e.data.toString();
		// console.log("Message from server", msg);
		// console.log(e);
		if (msg === "video.play") {
			status = "playing";
			// console.log("Starting player");
			player.play();
		} else if (msg === "video.pause") {
			status = "paused";
			// console.log("Pausing player");
			player.pause();
		} else if (msg.startsWith("video.seekMs.")) {
			let time = parseInt(msg.substr(13));
			player.currentTime(time / 1000);
		} else if (msg === "video.getMs") {
			let time = player.currentMs();
			ws.send(`video.seekMs.${time}`);
		} else if (msg.startsWith("video.name.")) {
			name = msg.substr(11);
			$(".user-name").text(name);
		} else if (msg.startsWith("video.others.")) {
			let others = JSON.parse(msg.substr(13));
			// console.log(others);
			let target = $("#others-list");
			target.html("");
			others.forEach((other) => {
				if (other != name) {
					target.append(`<li>${other}</li>`);
				}
			});
		}
	});

	ws.send("video.getMs");
}

function confirm() {
	console.log("confirm");
	$("#confirm").css("display", "none");

	ws = new WebSocket("ws://" + window.location.host);

	ws.addEventListener("open", (e) => {
		console.log("Opened connection!", e);
		setupSocket();
	});
}
