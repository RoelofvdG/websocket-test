const express = require("express");
const ws = require("ws");
const app = express();
const port = 3000;
const hri = require("human-readable-ids").hri;
const path = require("path");
const fs = require("fs");
const srt2vtt = require("srt-to-vtt");
const config = require("./config.json");

const server = app.listen(port, () => {
	console.log("Webserver listening at " + port);
});

const wsServer = new ws.Server({ server });

wsServer.broadcast = function (message) {
	wsServer.clients.forEach((client) => {
		client.send(message);
	});
};

wsServer.on("connection", (ws, req) => {
	// console.log("New connection");
	wsServer.broadcast("video.pause");
	ws.name = hri.random();
	ws.send("video.name." + ws.name);
	// console.log("sent name");
	let others = Array.from(wsServer.clients).map((x) => x.name);
	wsServer.broadcast("video.others." + JSON.stringify(others));

	ws.on("message", (msg) => {
		// console.log(msg);

		if (msg === "broadcast") {
			console.log("Received broadcast");
			wsServer.broadcast("Broadcast from srver");
		} else if (msg.startsWith("video.")) {
			msg = msg.substr(6);
			if (msg === "play") {
				wsServer.broadcast("video.play");
			} else if (msg === "pause") {
				wsServer.broadcast("video.pause");
			} else if (msg.startsWith("seekMs.")) {
				let time = parseInt(msg.substr(7));
				wsServer.broadcast("video.seekMs." + time);
			} else if (msg === "getMs") {
				wsServer.broadcast("video.pause");
				wsServer.clients.forEach((ws2) => {
					if (ws2 != ws) {
						ws2.send("video.getMs");
					}
				});
			}
		} else {
			ws.send("Reply from server");
		}
	});

	ws.on("close", (code, reason) => {
		// console.log("Websocket closed: ", reason);
		wsServer.broadcast("video.pause");
		let others = Array.from(wsServer.clients).map((x) => x.name);
		wsServer.broadcast("video.others." + JSON.stringify(others));
	});
});

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.static("public"));

app.get("/", async (req, res) => {
	let rejected = false;
	let content = await new Promise((accept, reject) => {
		fs.readdir(config.moviesPath, { withFileTypes: true }, (err, content) => {
			if (err) reject(`Failed to open directory: ${config.moviesPath}`);
			else accept(content);
			return;
		});
	}).catch((err) => {
		console.log(err);
		rejected = err;
	});

	if (rejected != false) {
		res.render("error", { message: rejected });
	} else {
		let videos = await Promise.all(
			content.map(
				(dirent) =>
					new Promise(async (accept, reject) => {
						if (!dirent.isDirectory()) {
							accept(null);
							return;
						}

						let checkContent = await new Promise(async (accept2, reject2) => {
							fs.readdir(
								path.join(config.moviesPath, dirent.name),
								{ withFileTypes: true },
								(err, checkContent) => {
									if (err) reject2(err);
									else accept2(checkContent);
									return;
								}
							);
						});
						if (
							checkContent.filter(
								(dirent) =>
									dirent.name.endsWith(".mp4") || dirent.name.endsWith(".webm")
							).length <= 0
						) {
							accept(null);
							return;
						}

						accept(dirent.name);
					})
			)
		);
		videos = videos.filter((x) => x != null);
		res.render("index", {
			videos,
		});
	}
});

app.get("/video", (req, res, next) => {
	let videoPath = req.query.file;
	if (!videoPath || !fs.existsSync(path.join(config.moviesPath, videoPath))) {
		next();
		return;
	} else {
		res.set("Cache-Control", "no-store");
		res.sendFile(path.join(config.moviesPath, videoPath));
	}
	// res.sendFile(process.cwd() + "/public/video/video.mp4");
});

app.get("/watch", async (req, res) => {
	if (!req.query || !req.query.video) {
		res.redirect("/");
		return;
	}
	const videoFolder = req.query.video;
	const videoPath = path.join(config.moviesPath, videoFolder);
	if (!fs.existsSync(videoPath)) {
		res.redirect("/");
		return;
	}

	let content = await new Promise((accept, reject) => {
		fs.readdir(videoPath, (err, files) => {
			if (err) reject(err);
			else accept(files);
		});
	});
	let videoFile = content.filter(
		(x) => x.endsWith(".mp4") || x.endsWith(".webm")
	);
	if (videoFile.length <= 0) {
		res.redirect("/");
		return;
	}
	videoFile = videoFile[0];

	const knownLanguages = [
		"english",
		"en",
		"nederlands",
		"nl",
		"dutch",
		"norwegian",
		"no",
		"swedish",
		"sv",
	];
	let subtitles = content
		.filter((x) => {
			const withoutExtension = x.slice(0, -4);
			const knownLanguage =
				knownLanguages.filter((x) =>
					withoutExtension.toLowerCase().endsWith("." + x)
				).length > 0;
			return (x.endsWith(".srt") || x.endsWith(".vtt")) && knownLanguage;
		})
		.map((sub) => {
			const subName = sub.slice(0, -4);
			let obj = { file: path.join(videoFolder, sub) };
			if (
				["nederlands", "nl", "dutch"].filter((x) =>
					subName.toLowerCase().endsWith("." + x)
				).length > 0
			) {
				obj.language = "Nederlands";
				obj.languageShort = "nl";
			} else if (
				["english", "en"].filter((x) => subName.toLowerCase().endsWith("." + x))
					.length > 0
			) {
				obj.language = "English";
				obj.languageShort = "en";
			} else if (
				["norwegian", "no"].filter((x) =>
					subName.toLowerCase().endsWith("." + x)
				).length > 0
			) {
				obj.language = "Norsk";
				obj.languageShort = "no";
			} else if (
				["swedish", "sv"].filter((x) => subName.toLowerCase().endsWith("." + x))
					.length > 0
			) {
				obj.language = "Svenska";
				obj.languageShort = "sv";
			}
			return obj;
		});

	res.render("watch", {
		videoPath: path.join(videoFolder, videoFile),
		videoFile,
		subtitles,
	});
});

app.get("/subtitle", (req, res, next) => {
	const subtitleFile = req.query.file;
	const subtitlePath = path.join(config.moviesPath, subtitleFile);
	if (!subtitleFile || !fs.existsSync(subtitlePath)) {
		res.redirect("/");
		return;
	}
	res.set("Cache-Control", "no-store");
	if (subtitlePath.toLowerCase().endsWith(".vtt")) {
		res.sendFile(subtitlePath);
	} else if (subtitlePath.toLowerCase().endsWith(".srt")) {
		fs.createReadStream(subtitlePath).pipe(srt2vtt()).pipe(res);
	} else {
		next();
	}
});
